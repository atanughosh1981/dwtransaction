/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.service.dwtransaction.web.rest.vm;
